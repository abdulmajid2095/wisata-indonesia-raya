package com.elecomp.wisataindonesiaraya;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Halaman2 extends Activity implements OnClickListener{

	LinearLayout menubar;
	ImageButton menu,cari;
	int status=1;
	
	TextView namawisata,deskripsiwisata;
	ImageView gambar;
	Button tombolhome,tombolreload,tomboltentang,tombolkeluar,tombolwisata,tombolartikel;
	
	private static String TAG = MainActivity.class.getSimpleName();
	private ProgressDialog pDialog;
	String idku,urlJsonArry;
	
	String urlgbr,lokasi,nama,deskripsi,alamat;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_halaman2);
		
		Intent detail = getIntent();
		idku = detail.getStringExtra("datakirim");
		urlJsonArry = "http://wisataindonesiaraya.com/getData.php?menu=5&id="+idku;
		
		menubar = (LinearLayout) findViewById(R.id.menubarhalaman2);
		
		menu = (ImageButton) findViewById(R.id.menuhalaman2);
		menu.setOnClickListener(this);
		
		
		namawisata = (TextView) findViewById(R.id.homenamawisatahalaman2);
		deskripsiwisata = (TextView)findViewById(R.id.homedeskripsiwisatahalaman2);
		gambar = (ImageView) findViewById(R.id.homegambarhalaman2);
		
		tombolhome = (Button) findViewById(R.id.tombolhomehalaman2);
		tombolhome.setOnClickListener(this);
		tombolreload = (Button) findViewById(R.id.reloadhalaman2);
		tombolreload.setOnClickListener(this);
		tomboltentang = (Button) findViewById(R.id.tomboltentanghalaman2);
		tomboltentang.setOnClickListener(this);
		tombolkeluar = (Button) findViewById(R.id.tombolkeluarhalaman2);
		tombolkeluar.setOnClickListener(this);
		tombolwisata = (Button) findViewById(R.id.tombolwisatahalaman2);
		tombolwisata.setOnClickListener(this);
		tombolartikel = (Button) findViewById(R.id.tombolartikelhalaman2);
		tombolartikel.setOnClickListener(this);
		cari = (ImageButton) findViewById(R.id.tombolcarihalaman2);
		cari.setOnClickListener(this);
		

		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);
				
        ambildata();
     
	}
	@Override
	public void onClick(View pilihan) {
		
		switch (pilihan.getId()) {
		
		case R.id.menuhalaman2:
			
			if(status==1)
			{
				tampilmenu();
			}
			if(status==-1)
			{
				hilangmenu();
			}
		status=-status;
		break;
		
		case R.id.tombolwisatahalaman2:
			Intent wisata = new Intent(Halaman2.this, Wisata.class);
			startActivity(wisata);
		break;
		
		case R.id.tombolartikelhalaman2:
			Intent artikel = new Intent(Halaman2.this, Artikel.class);
			startActivity(artikel);
		break;
		
		case R.id.tombolcarihalaman2:
			Intent cari = new Intent(Halaman2.this, Pencarian.class);
			startActivity(cari);
		break;
		
		case R.id.tombolhomehalaman2:
			Intent home = new Intent(Halaman2.this, Utama.class);
			startActivity(home);
		break;
		
		case R.id.reloadhalaman2:
			ambildata();
		break;
		
		case R.id.tomboltentanghalaman2:
			Intent tentang = new Intent(Halaman2.this, Tentang.class);
			startActivity(tentang);
		break;
		
		case R.id.tombolkeluarhalaman2:
			keluar();
		break;
		
		}
		
		
	}
	
	public void tampilmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 258, 2);
		animasi.setDuration(300);
		animasi.setFillAfter(true);
		menubar.startAnimation(animasi);
	}
	
	public void hilangmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 2, 858);
		animasi.setDuration(700);
		menubar.startAnimation(animasi);
		//menubar.setVisibility(View.GONE);
		
	}
	
	
	private void ambildata() {
		
		
        JsonArrayRequest req = new JsonArrayRequest(urlJsonArry,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject wisata = (JSONObject) response
                                        .get(0);
 
                                nama = wisata.getString("judul");
                                deskripsi = wisata.getString("isi_artikel");
                                urlgbr = wisata.getString("gambar");
                                namawisata.setText(""+nama);
                                //deskripsiwisata.setText(""+deskripsi);
                                deskripsiwisata.setText(Html.fromHtml(""+deskripsi));
                                loadgambar();
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        showpDialog();
    }
 
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
	
	
    public void onBackPressed()
    {

       super.onBackPressed(); // Comment this super call to avoid calling finish()
    }
	
    
    public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(intent);
			    Intent keluar = new Intent(Halaman2.this, MainActivity.class);
				startActivity(keluar);
			    android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		dialogkeluar.show();
    }

    public void loadgambar()
    {
    	Picasso.with(this)
        .load("http://wisataindonesiaraya.com/admin/img_foto/ARTIKEL/"+urlgbr)
        .resize(300, 200)
        .centerCrop()
        .into(gambar);
    }
    
    
}
