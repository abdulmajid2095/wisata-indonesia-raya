package com.elecomp.wisataindonesiaraya;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Utama extends Activity implements OnClickListener{

	Button tombolhome,tombolreload,tomboltentang,tombolkeluar,tombolwisata,tombolartikel,detailwisatahome,detailartikelhome;
	ImageButton menu,cari;
	TextView textku,namawisatahome,namaartikelhome;
	int status=1;
	LinearLayout menubar;
	ImageView gambarwisatahome,gambarartikelhome;
	
	private ProgressDialog pDialog;
	private static String TAG = MainActivity.class.getSimpleName();
	String urlJson="http://wisataindonesiaraya.com/getData.php?menu=3&pilihan=1",nama,deskripsi,urlgbr,id;
	String urlJson2="http://wisataindonesiaraya.com/getData.php?menu=3&pilihan=2",nama2,deskripsi2,urlgbr2,id2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_utama);
		
		tombolhome = (Button) findViewById(R.id.tombolhomeutama);
		tombolhome.setOnClickListener(this);
		tombolreload = (Button) findViewById(R.id.reloadutama);
		tombolreload.setOnClickListener(this);
		tomboltentang = (Button) findViewById(R.id.tomboltentangutama);
		tomboltentang.setOnClickListener(this);
		tombolkeluar = (Button) findViewById(R.id.tombolkeluarutama);
		tombolkeluar.setOnClickListener(this);
		menu = (ImageButton) findViewById(R.id.menuutama);
		menu.setOnClickListener(this);
		tombolwisata = (Button) findViewById(R.id.tombolwisatautama);
		tombolwisata.setOnClickListener(this);
		tombolartikel = (Button) findViewById(R.id.tombolartikelutama);
		tombolartikel.setOnClickListener(this);
		cari = (ImageButton) findViewById(R.id.tombolcariutama);
		cari.setOnClickListener(this);
		menubar = (LinearLayout) findViewById(R.id.menubarutama);
		menubar.setVisibility(View.INVISIBLE);
		
		
		namawisatahome=(TextView)findViewById(R.id.namawisatahome);
		namaartikelhome=(TextView) findViewById(R.id.namaartikelhome);
		gambarartikelhome=(ImageView) findViewById(R.id.gambarartikelhome);
		gambarwisatahome=(ImageView) findViewById(R.id.gambarwisatahome);
		detailartikelhome=(Button)findViewById(R.id.detailartikelhome);
		detailartikelhome.setOnClickListener(this);
		detailwisatahome=(Button)findViewById(R.id.detailwisatahome);
		detailwisatahome.setOnClickListener(this);
		
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);
			
		ambildata();
		ambildata2();
	}
	@Override
	public void onClick(View pilihan) {		
		switch (pilihan.getId()) {
		case R.id.menuutama:
			
			if(status==1)
			{
				tampilmenu();
			}
			if(status==-1)
			{
				hilangmenu();
			}
		status=-status;
		break;
		
		case R.id.tombolwisatautama:
			Intent wisata = new Intent(Utama.this, Wisata.class);
			startActivity(wisata);
		break;
		
		case R.id.tombolartikelutama:
			Intent artikel = new Intent(Utama.this, Artikel.class);
			startActivity(artikel);
		break;
		
		case R.id.tombolhomeutama:
			Intent home = new Intent(Utama.this, Utama.class);
			startActivity(home);
		break;
		
		case R.id.reloadutama:
			ambildata();
			ambildata2();
		break;
		
		case R.id.tomboltentangutama:
			Intent tentang = new Intent(Utama.this, Tentang.class);
			startActivity(tentang);
		break;
		
		case R.id.tombolkeluarutama:
			keluar();
		break;
		
		case R.id.tombolcariutama:
			Intent cari = new Intent(Utama.this, Pencarian.class);
			startActivity(cari);
		break;
		
		case R.id.detailartikelhome:
			Intent detail = new Intent(Utama.this, Halaman2.class);
			detail.putExtra("datakirim", ""+id2);
			startActivity(detail);
		break;
		
		case R.id.detailwisatahome:
			Intent detail2 = new Intent(Utama.this, Halaman.class);
			detail2.putExtra("datakirim", ""+id);
			startActivity(detail2);
		break;
		}
	}

	
	public void tampilmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 258, 2);
		animasi.setDuration(300);
		animasi.setFillAfter(true);
		menubar.startAnimation(animasi);
	}
	
	public void hilangmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 2, 858);
		animasi.setDuration(700);
		menubar.startAnimation(animasi);
		//menubar.setVisibility(View.GONE);
		
	}
	
	
	public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				/*Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(int	ent);
			    Intent keluar = new Intent(Utama.this, MainActivity.class);
				startActivity(keluar);
			    android.os.Process.killProcess(android.os.Process.myPid());*/
				finish();
				moveTaskToBack(true);
				finish();
			}
		});
		dialogkeluar.show();
    }
	
	
	
	
	public void onBackPressed()
    {

       // super.onBackPressed(); // Comment this super call to avoid calling finish()
    }

	
	private void ambildata() {

        JsonArrayRequest req = new JsonArrayRequest(urlJson,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject wisata = (JSONObject) response
                                        .get(0);
 
                                nama = wisata.getString("nama_wisata");
                                urlgbr = wisata.getString("foto1_wisata");
                                id=wisata.getString("id_wisata");
                                namawisatahome.setText(""+nama);
                                loadgambar();
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        showpDialog();  
    }

	private void showpDialog() {
	    if (!pDialog.isShowing())
	        pDialog.show();
	}
	
	private void hidepDialog() {
	    if (pDialog.isShowing())
	        pDialog.dismiss();
	}
	
	public void loadgambar()
    {
    	Picasso.with(this)
        .load("http://wisataindonesiaraya.com/admin/img_foto/"+urlgbr)
        .error(R.drawable.error)
        .resize(110, 100)
        .centerCrop()
        .into(gambarwisatahome);
    }
	
	
	
	
	private void ambildata2() {

        JsonArrayRequest req = new JsonArrayRequest(urlJson2,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject artikel = (JSONObject) response
                                        .get(0);
 
                                nama2 = artikel.getString("judul");
                                urlgbr2 = artikel.getString("gambar");
                                id2=artikel.getString("id_artikel");
                                namaartikelhome.setText(""+nama2);
                                loadgambar2();
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        showpDialog();  
    }
	
	public void loadgambar2()
    {
    	Picasso.with(this)
        .load("http://wisataindonesiaraya.com/admin/img_foto/ARTIKEL/"+urlgbr2)
        .error(R.drawable.error)
        .resize(110, 100)
        .centerCrop()
        .into(gambarartikelhome);
    }
	
}




