package com.elecomp.wisataindonesiaraya;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Peta extends Activity implements OnClickListener{

	Button tombolhome,tombolreload,tomboltentang,tombolkeluar,tombolwisata,tombolartikel;
	ImageButton menu,cari;
	TextView textku,cobateks;
	int status=1;
	LinearLayout menubar;
	WebView myWebView;
	String petaku,cek="0,0",zindex=",16z",lokasi;
	
	private ProgressDialog pDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_peta);
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading Peta");
        pDialog.setCancelable(true);
		
		tombolhome = (Button) findViewById(R.id.tombolhomepeta);
		tombolhome.setOnClickListener(this);
		tombolreload = (Button) findViewById(R.id.reloadpeta);
		tombolreload.setOnClickListener(this);
		tomboltentang = (Button) findViewById(R.id.tomboltentangpeta);
		tomboltentang.setOnClickListener(this);
		tombolkeluar = (Button) findViewById(R.id.tombolkeluarpeta);
		tombolkeluar.setOnClickListener(this);
		menu = (ImageButton) findViewById(R.id.menupeta);
		menu.setOnClickListener(this);
		tombolwisata = (Button) findViewById(R.id.tombolwisatapeta);
		tombolwisata.setOnClickListener(this);
		tombolartikel = (Button) findViewById(R.id.tombolartikelpeta);
		tombolartikel.setOnClickListener(this);
		cari = (ImageButton) findViewById(R.id.tombolcaripeta);
		cari.setOnClickListener(this);
		menubar = (LinearLayout) findViewById(R.id.menubarpeta);
		menubar.setVisibility(View.INVISIBLE);
		
		showpDialog();
		
		
		Intent baru = getIntent();
		petaku = baru.getStringExtra("datakirim");
		
		loadpeta();
			
	}
	@Override
	public void onClick(View pilihan) {		
		switch (pilihan.getId()) {
		case R.id.menupeta:
			
			if(status==1)
			{
				tampilmenu();
			}
			if(status==-1)
			{
				hilangmenu();
			}
		status=-status;
		break;
		
		case R.id.tombolwisatapeta:
			Intent wisata = new Intent(Peta.this, Wisata.class);
			startActivity(wisata);
		break;
		
		case R.id.tombolartikelpeta:
			Intent artikel = new Intent(Peta.this, Artikel.class);
			startActivity(artikel);
		break;
		
		case R.id.tombolhomepeta:
			Intent baru = new Intent(Peta.this, Utama.class);
			startActivity(baru);
		break;
		
		case R.id.reloadpeta:
			loadpeta();
		break;
		
		case R.id.tomboltentangpeta:
			Intent tentang = new Intent(Peta.this, Tentang.class);
			startActivity(tentang);
		break;
		
		case R.id.tombolkeluarpeta:
			keluar();
		break;
		
		case R.id.tombolcaripeta:
			Intent cari = new Intent(Peta.this, Pencarian.class);
			startActivity(cari);
		break;
		
		
		}
	}

	
	public void tampilmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 258, 2);
		animasi.setDuration(300);
		animasi.setFillAfter(true);
		menubar.startAnimation(animasi);
	}
	
	public void hilangmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 2, 858);
		animasi.setDuration(700);
		menubar.startAnimation(animasi);
		//menubar.setVisibility(View.GONE);
		
	}
	
	
	public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(intent);
			    Intent keluar = new Intent(Peta.this, MainActivity.class);
				startActivity(keluar);
			    android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		dialogkeluar.show();
    }
	
	
	private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    public void loadpeta()
    {
    	
    	if (petaku.equals(cek)){        
         	petaku="-1.2468327,117.886753";
         	zindex=",7z";
         }
    	
    	myWebView = (WebView) findViewById(R.id.peta);
    	lokasi="https://www.google.co.id/maps/@"+petaku+zindex+"?hl=en";
    	myWebView.loadUrl(lokasi);
		WebSettings webSettings = myWebView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		
		myWebView.setWebViewClient(new WebViewClient() {

			   public void onPageFinished(WebView view, String url) {
			        hidepDialog();
			    }
			});
    }
}
