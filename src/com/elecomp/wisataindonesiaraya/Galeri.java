package com.elecomp.wisataindonesiaraya;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Galeri extends Activity implements OnClickListener{

	Button tombolhome,tombolreload,tomboltentang,tombolkeluar,tombolwisata,tombolartikel,detailwisatahome,detailartikelhome;
	ImageButton menu,cari;
	TextView textku,namawisatagaleri;
	int status=1;
	LinearLayout menubar;
	ImageView gambargaleri1,gambargaleri2,gambargaleri3;
	
	private ProgressDialog pDialog;
	private static String TAG = MainActivity.class.getSimpleName();
	String urlJson,id,gbr1,gbr2,gbr3,nama;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_galeri);
		
		Intent galeri = getIntent();
		id = galeri.getStringExtra("datakirim");
		urlJson="http://wisataindonesiaraya.com/getData.php?menu=1&id="+id;
		
		
		tombolhome = (Button) findViewById(R.id.tombolhomegaleri);
		tombolhome.setOnClickListener(this);
		tombolreload = (Button) findViewById(R.id.reloadgaleri);
		tombolreload.setOnClickListener(this);
		tomboltentang = (Button) findViewById(R.id.tomboltentanggaleri);
		tomboltentang.setOnClickListener(this);
		tombolkeluar = (Button) findViewById(R.id.tombolkeluargaleri);
		tombolkeluar.setOnClickListener(this);
		menu = (ImageButton) findViewById(R.id.menugaleri);
		menu.setOnClickListener(this);
		tombolwisata = (Button) findViewById(R.id.tombolwisatagaleri);
		tombolwisata.setOnClickListener(this);
		tombolartikel = (Button) findViewById(R.id.tombolartikelgaleri);
		tombolartikel.setOnClickListener(this);
		cari = (ImageButton) findViewById(R.id.tombolcarigaleri);
		cari.setOnClickListener(this);
		menubar = (LinearLayout) findViewById(R.id.menubargaleri);
		menubar.setVisibility(View.INVISIBLE);
		
		gambargaleri1 = (ImageView)findViewById(R.id.gambargaleri1);
		gambargaleri2 = (ImageView)findViewById(R.id.gambargaleri2);
		gambargaleri3 = (ImageView)findViewById(R.id.gambargaleri3);
		
		namawisatagaleri=(TextView)findViewById(R.id.namawisatagaleri);
		
		
		
		
		
		
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);
			
		ambildata();
	}
	@Override
	public void onClick(View pilihan) {		
		switch (pilihan.getId()) {
		case R.id.menugaleri:
			
			if(status==1)
			{
				tampilmenu();
			}
			if(status==-1)
			{
				hilangmenu();
			}
		status=-status;
		break;
		
		case R.id.tombolwisatagaleri:
			Intent wisata = new Intent(Galeri.this, Wisata.class);
			startActivity(wisata);
		break;
		
		case R.id.tombolartikelgaleri:
			Intent artikel = new Intent(Galeri.this, Artikel.class);
			startActivity(artikel);
		break;
		
		case R.id.tombolhomegaleri:
			Intent home = new Intent(Galeri.this, Utama.class);
			startActivity(home);
		break;
		
		case R.id.reloadgaleri:
			ambildata();

		break;
		
		case R.id.tomboltentanggaleri:
			Intent tentang = new Intent(Galeri.this, Tentang.class);
			startActivity(tentang);
		break;
		
		case R.id.tombolkeluargaleri:
			keluar();
		break;
		
		case R.id.tombolcarigaleri:
			Intent cari = new Intent(Galeri.this, Pencarian.class);
			startActivity(cari);
		break;
		}
	}

	
	public void tampilmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 258, 2);
		animasi.setDuration(300);
		animasi.setFillAfter(true);
		menubar.startAnimation(animasi);
	}
	
	public void hilangmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 2, 858);
		animasi.setDuration(700);
		menubar.startAnimation(animasi);
		//menubar.setVisibility(View.GONE);
		
	}
	
	
	public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(intent);
			    Intent keluar = new Intent(Galeri.this, MainActivity.class);
				startActivity(keluar);
			    android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		dialogkeluar.show();
    }
	

	
	private void ambildata() {
	
        JsonArrayRequest req = new JsonArrayRequest(urlJson,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject wisata = (JSONObject) response
                                        .get(0);
                                
                                nama = wisata.getString("nama_wisata");
                                namawisatagaleri.setText(""+nama);
                                gbr1 = wisata.getString("foto1_wisata");
                                gbr2 = wisata.getString("foto2_wisata");
                                gbr3 = wisata.getString("foto3_wisata");
                                
                                loadgambar1();
                                loadgambar2();
                                loadgambar3();
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        showpDialog();  
    }

	private void showpDialog() {
	    if (!pDialog.isShowing())
	        pDialog.show();
	}
	
	private void hidepDialog() {
	    if (pDialog.isShowing())
	        pDialog.dismiss();
	}
	
	public void loadgambar1()
    {
    	Picasso.with(this)
        .load("http://wisataindonesiaraya.com/admin/img_foto/"+gbr1)
        .error(R.drawable.error)
        .resize(400, 300)
        .centerCrop()
        .into(gambargaleri1);
    }
	public void loadgambar2()
    {
    	Picasso.with(this)
        .load("http://wisataindonesiaraya.com/admin/img_foto/"+gbr2)
        .error(R.drawable.error)
        .resize(400, 300)
        .centerCrop()
        .into(gambargaleri2);
    }
	public void loadgambar3()
    {
    	Picasso.with(this)
        .load("http://wisataindonesiaraya.com/admin/img_foto/"+gbr3)
        .error(R.drawable.error)
        .resize(400, 300)
        .centerCrop()
        .into(gambargaleri3);
    }
	
}




