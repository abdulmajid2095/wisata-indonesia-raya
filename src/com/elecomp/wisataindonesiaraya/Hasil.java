package com.elecomp.wisataindonesiaraya;


import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class Hasil extends Activity implements OnClickListener{

	Button tombolhome,tombolreload,tomboltentang,tombolkeluar,tombolwisata,tombolartikel,selanjutya;
	ImageButton menu,cari;
	TextView textku,halamana,halamanb,judulhasil;
	int status=1;
	LinearLayout menubar;
	String urlJsonArry;
	private static String TAG = MainActivity.class.getSimpleName();
	private ProgressDialog pDialog;
	int jumlahdata,i,paging,dataawal=0,dataakhir=10,halaman=1;
	JSONObject wisata;
	LinearLayout layouthasil;
	String[] nama;
	String[] urlgbr;
	String[] id;
	String judul,namacari="";
	String pilihan,cek="semua";

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_hasil);
		
		Intent baru = getIntent();
		judul = baru.getStringExtra("datakirim");
		
		Intent kecari = getIntent();
		namacari = kecari.getStringExtra("pencarian");
		judulhasil=(TextView)findViewById(R.id.judulhasil);
		
		if(judul==null)
		{
			judulhasil.setText("Hasil Ditemukan");
			judul="";
		}
		else if(judul!=null)
		{
			namacari="";
			judulhasil.setText("Wisata "+judul);
			
		}
	
		urlJsonArry = "http://wisataindonesiaraya.com/getData.php?menu=2&wisata="+judul+"&cari="+namacari;
		
		tombolhome = (Button) findViewById(R.id.tombolhomehasil);
		tombolhome.setOnClickListener(this);
		tombolreload = (Button) findViewById(R.id.reloadhasil);
		tombolreload.setOnClickListener(this);
		tomboltentang = (Button) findViewById(R.id.tomboltentanghasil);
		tomboltentang.setOnClickListener(this);
		tombolkeluar = (Button) findViewById(R.id.tombolkeluarhasil);
		tombolkeluar.setOnClickListener(this);
		menu = (ImageButton) findViewById(R.id.menuhasil);
		menu.setOnClickListener(this);
		tombolwisata = (Button) findViewById(R.id.tombolwisatahasil);
		tombolwisata.setOnClickListener(this);
		tombolartikel = (Button) findViewById(R.id.tombolartikelhasil);
		tombolartikel.setOnClickListener(this);
		cari = (ImageButton) findViewById(R.id.tombolcarihasil);
		cari.setOnClickListener(this);
		selanjutya = (Button) findViewById(R.id.selanjutnya);
		selanjutya.setOnClickListener(this);
		menubar = (LinearLayout) findViewById(R.id.menubarhasil);
		menubar.setVisibility(View.INVISIBLE);
		
		halamana=(TextView)findViewById(R.id.halamana);
		halamanb=(TextView)findViewById(R.id.halamanb);
		
		layouthasil = (LinearLayout) findViewById(R.id.layouthasil);
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);
		
        halamana.setText(""+halaman);
        
        ambildata();
       
	}
	@Override
	public void onClick(View pilihan) {		
		switch (pilihan.getId()) {
		case R.id.menuhasil:
			
			if(status==1)
			{
				tampilmenu();
			}
			if(status==-1)
			{
				hilangmenu();
			}
		status=-status;
		break;
		
		case R.id.tombolwisatahasil:
			Intent wisata = new Intent(Hasil.this, Wisata.class);
			startActivity(wisata);
		break;
		
		case R.id.tombolartikelhasil:
			Intent artikel = new Intent(Hasil.this, Artikel.class);
			startActivity(artikel);
		break;
		
		case R.id.tombolcarihasil:
			Intent cari = new Intent(Hasil.this, Pencarian.class);
			startActivity(cari);
		break;
		
		case R.id.tombolhomehasil:
			Intent home = new Intent(Hasil.this, Utama.class);
			startActivity(home);
		break;
		
		case R.id.reloadhasil:
			ambildata();
		break;
		
		case R.id.tomboltentanghasil:
			Intent tentang = new Intent(Hasil.this, Tentang.class);
			startActivity(tentang);
		break;
		
		case R.id.tombolkeluarhasil:
			keluar();
		break;

		
		case R.id.selanjutnya:
			if(halaman<paging)
			{
				halaman=halaman+1;
				dataawal=dataawal+10;
				dataakhir=dataakhir+10;
				ambildata();
			}
		break;
		
		}
	}

	
	public void tampilmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 258, 2);
		animasi.setDuration(300);
		animasi.setFillAfter(true);
		menubar.startAnimation(animasi);
	}
	
	public void hilangmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 2, 858);
		animasi.setDuration(700);
		menubar.startAnimation(animasi);
		//menubar.setVisibility(View.GONE);
		
	}
	
	
	public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(intent);
			    Intent keluar = new Intent(Hasil.this, MainActivity.class);
				startActivity(keluar);
			    android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		dialogkeluar.show();
    }
	
	
	
	private void ambildata() {
        JsonArrayRequest req = new JsonArrayRequest(urlJsonArry,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	jumlahdata = response.length();
                            paging=(jumlahdata/10)+1;
                            if(jumlahdata<dataakhir)
                            {
                            	halamana.setText(""+jumlahdata);
                            }
                            else{
                            	halamana.setText(""+dataakhir);
                            }
                            halamanb.setText(""+jumlahdata);
                            
                        	nama = new String[jumlahdata];
                        	urlgbr = new String[jumlahdata];
                        	id = new String[jumlahdata];
                        	
                        	//for (i = 0; i < response.length(); i++) {
                        	for (i = dataawal; i < dataakhir; i++) {
                                wisata = (JSONObject) response
                                        .get(i);
                                
                                	nama[i] = wisata.getString("nama_wisata");
                                    urlgbr[i] = wisata.getString("foto1_wisata");
                                    id[i] = wisata.getString("id_wisata");

                                looping();
                                
                        	}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            /*Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                            if(jumlahdata==0)
                        	{
                        		peringatan2();
                        	}
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();*/
                    	if(jumlahdata==0)
                    	{
                    		peringatan2();
                    	}
                    	else{
                    		peringatan();
                    	}
                        
                    }
                });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        showpDialog();
        
        
    }
 
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    
    
    
    public void looping()
    {
    	
    	//LinearLayout Utama
    	LinearLayout layout1 = new LinearLayout(this);
    	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
    	params.bottomMargin=20;
    	params.height=0;
    	params.weight=1.0f;
    	//params.height=130;
    	//params.width=300;
    	params.gravity = Gravity.CENTER;
    	layout1.setLayoutParams(params);
		layout1.setBackgroundColor(Color.rgb(227, 239, 255));
		layout1.setPadding(10, 10, 10, 10);
		
		//Layout Gambar
		LinearLayout layoutgambar = new LinearLayout(this);
    	LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
    	params2.width=0;
    	params2.weight = 4.0f;
    	params2.gravity = Gravity.CENTER;
    	layoutgambar.setLayoutParams(params2);
		
    	//Gambar
		LinearLayout.LayoutParams GambarParams = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		ImageView gambar = new ImageView(this);
    	GambarParams.gravity = Gravity.CENTER;
		gambar.setScaleType(ScaleType.CENTER_CROP);
		gambar.setLayoutParams(GambarParams);
		
		
		Picasso.with(this)
        .load("http://wisataindonesiaraya.com/admin/img_foto/"+urlgbr[i])
        .resize(110, 100)
        .centerCrop()
        .into(gambar);
		
		
		//Layout Nama dan Tombol
		LinearLayout layout2 = new LinearLayout(this);
		LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		params3.width=0;
		params3.weight = 6.0f;
		params3.gravity = Gravity.CENTER;
		params3.leftMargin=10;
		layout2.setLayoutParams(params3);
		layout2.setOrientation(LinearLayout.VERTICAL);
		
		//LayoutNama
		LinearLayout layoutnama = new LinearLayout(this);
		LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 0);
		params4.weight = 3.0f;
		params4.gravity = Gravity.CENTER;
		layoutnama.setLayoutParams(params4);
		
		//NamaWisata
		TextView namawisata = new TextView(this);
		namawisata.setText(""+nama[i]);
		namawisata.setTextColor(Color.WHITE);
		namawisata.setTextSize(18);
		namawisata.setTextColor(Color.rgb(64,74,112));
		LinearLayout.LayoutParams params4a = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT );
		params4a.gravity = Gravity.CENTER;
		namawisata.setLayoutParams(params4a);
		
		//LayoutTombol
		LinearLayout layouttombol = new LinearLayout(this);
		LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 0);
		params5.weight = 2.0f;
		params5.gravity = Gravity.CENTER;
		layouttombol.setLayoutParams(params5);
		
		//LayoutTombol1a
		LinearLayout layouttombol1a = new LinearLayout(this);
		LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT);
		params6.weight = 1.0f;
		params6.gravity = Gravity.CENTER;
		layouttombol1a.setLayoutParams(params6);
		
		//LayoutTombol1b
		LinearLayout layouttombol1b = new LinearLayout(this);
		LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT);
		params7.weight = 1.0f;
		params7.gravity = Gravity.CENTER;
		layouttombol1b.setLayoutParams(params7);
		
		
		LinearLayout.LayoutParams paramsbtn = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		paramsbtn.gravity=Gravity.CENTER;
		Button btn = new Button(this);
		//btn.setId(id[i]);
		final String idku = ""+id[i];
		btn.setText("Detail");
		btn.setBackgroundColor(R.drawable.tombolmenu);
		btn.setLayoutParams(paramsbtn);
		btn.setOnClickListener(new OnClickListener() {
	        public void onClick(View v) {
	        	
	        	Intent detail = new Intent(Hasil.this, Halaman.class);
				detail.putExtra("datakirim", ""+idku);
				startActivity(detail);
	            
	        }
	    });
		
		
		layouthasil.addView(layout1);
		layout1.addView(layoutgambar);
		layout1.addView(layout2);
		layout2.addView(layoutnama);
		layout2.addView(layouttombol);
		layoutnama.addView(namawisata);
		layoutgambar.addView(gambar);
		layouttombol.addView(layouttombol1a);
		layouttombol.addView(layouttombol1b);
		layouttombol1b.addView(btn);
    	
    	//}
    }
    
    
    public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KONEKSI");
		dialogkeluar.setMessage("Koneksi Gagal");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
    
    public void peringatan2()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("Data");
		dialogkeluar.setMessage("Data Tidak Ditemukan");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
    
    
    
    

}


