package com.elecomp.wisataindonesiaraya;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Halaman extends Activity implements OnClickListener{

	LinearLayout menubar;
	ImageButton menu,suka,tombolpeta,cari,tombolgaleri;
	int status=1,statussuka=1;
	
	TextView namawisata,deskripsiwisata,alamatwisata;
	ImageView gambar;
	Button tombolhome,tombolreload,tomboltentang,tombolkeluar,tombolwisata,tombolartikel;
	
	private static String TAG = MainActivity.class.getSimpleName();
	private ProgressDialog pDialog;
	String idku,urlJsonArry;
	
	String urlgbr,latitude,longtitude,nama,deskripsi,alamat,lokasi,cek="0";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_halaman);
		
		Intent detail = getIntent();
		idku = detail.getStringExtra("datakirim");
		urlJsonArry = "http://wisataindonesiaraya.com/getData.php?menu=1&id="+idku;
		
		menubar = (LinearLayout) findViewById(R.id.menubar);
		
		menu = (ImageButton) findViewById(R.id.menu);
		menu.setOnClickListener(this);
		
		/*suka = (ImageButton) findViewById(R.id.tombolsuka);
		suka.setOnClickListener(this);*/
		
		namawisata = (TextView) findViewById(R.id.homenamawisata);
		deskripsiwisata = (TextView)findViewById(R.id.homedeskripsiwisata);
		//alamatwisata = (TextView) findViewById(R.id.alamat);
		gambar = (ImageView) findViewById(R.id.homegambar);
		
		tombolhome = (Button) findViewById(R.id.tombolhome);
		tombolhome.setOnClickListener(this);
		tombolreload = (Button) findViewById(R.id.reload);
		tombolreload.setOnClickListener(this);
		tomboltentang = (Button) findViewById(R.id.tomboltentang);
		tomboltentang.setOnClickListener(this);
		tombolkeluar = (Button) findViewById(R.id.tombolkeluar);
		tombolkeluar.setOnClickListener(this);
		tombolpeta = (ImageButton) findViewById(R.id.tombolpeta);
		tombolpeta.setOnClickListener(this);
		tombolgaleri = (ImageButton) findViewById(R.id.tombolgaleri);
		tombolgaleri.setOnClickListener(this);
		tombolwisata = (Button) findViewById(R.id.tombolwisata);
		tombolwisata.setOnClickListener(this);
		tombolartikel = (Button) findViewById(R.id.tombolartikel);
		tombolartikel.setOnClickListener(this);
		cari = (ImageButton) findViewById(R.id.tombolcari);
		cari.setOnClickListener(this);
		menubar.setVisibility(View.INVISIBLE);
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);
				
        ambildata();
     
	}
	@Override
	public void onClick(View pilihan) {
		
		switch (pilihan.getId()) {
		
		case R.id.menu:
			
			if(status==1)
			{
				tampilmenu();
			}
			if(status==-1)
			{
				hilangmenu();
			}
		status=-status;
		break;
		
		case R.id.tombolwisata:
			Intent wisata = new Intent(Halaman.this, Wisata.class);
			startActivity(wisata);
		break;
		
		case R.id.tombolartikel:
			Intent artikel = new Intent(Halaman.this, Artikel.class);
			startActivity(artikel);
		break;
		
		case R.id.tombolcari:
			Intent cari = new Intent(Halaman.this, Pencarian.class);
			startActivity(cari);
		break;
		
		case R.id.tombolhome:
			Intent home = new Intent(Halaman.this, Utama.class);
			startActivity(home);
		break;
		
		case R.id.reload:
			ambildata();
		break;
		
		case R.id.tomboltentang:
			Intent tentang = new Intent(Halaman.this, Tentang.class);
			startActivity(tentang);
		break;
		
		case R.id.tombolkeluar:
			keluar();
		break;
		
		/*case R.id.tombolsuka:
			statussuka=-statussuka;
			if(statussuka==-1)
			{
				suka.setImageResource(R.drawable.tblsuka2);
			}
			if(statussuka==1)
			{
				suka.setImageResource(R.drawable.tblsuka1);
			}
		break;*/
		
		case R.id.tombolpeta:
			Intent kepeta = new Intent(Halaman.this, Peta.class);
			kepeta.putExtra("datakirim", ""+lokasi);
			startActivity(kepeta);
		break;
		
		case R.id.tombolgaleri:
			Intent kegaleri = new Intent(Halaman.this, Galeri.class);
			kegaleri.putExtra("datakirim", ""+idku);
			startActivity(kegaleri);
		break;
		
		
		}
		
		
	}
	
	public void tampilmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 258, 2);
		animasi.setDuration(300);
		animasi.setFillAfter(true);
		menubar.startAnimation(animasi);
	}
	
	public void hilangmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 2, 858);
		animasi.setDuration(700);
		menubar.startAnimation(animasi);
		//menubar.setVisibility(View.GONE);
		
	}
	
	
	private void ambildata() {
		
		
        JsonArrayRequest req = new JsonArrayRequest(urlJsonArry,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject wisata = (JSONObject) response
                                        .get(0);
 
                                nama = wisata.getString("nama_wisata");
                                deskripsi = wisata.getString("deskripsi_wisata");
                                latitude = wisata.getString("latitude");
                                longtitude = wisata.getString("longtitude");
                                
                                lokasi=latitude+","+longtitude;
                                urlgbr = wisata.getString("foto1_wisata");
                                
                                namawisata.setText(""+nama);
                                deskripsiwisata.setText(Html.fromHtml("<p align=\"justify\">"+deskripsi+"</P>"));
                                //deskripsiwisata.setText(""+deskripsi);
                                //alamatwisata.setText(""+alamat);
                                loadgambar();
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        showpDialog();
    }
 
    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }
 
    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
	
	
    public void onBackPressed()
    {

       super.onBackPressed(); // Comment this super call to avoid calling finish()
    }
	
    
    public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(intent);
			    Intent keluar = new Intent(Halaman.this, MainActivity.class);
				startActivity(keluar);
			    android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		dialogkeluar.show();
    }

    public void loadgambar()
    {
    	Picasso.with(this)
        .load("http://wisataindonesiaraya.com/admin/img_foto/"+urlgbr)
        .error(R.drawable.error)
        .resize(300, 200)
        .centerCrop()
        .into(gambar);
    }
    
    
    
    
    
private void komentar() {
		
		String urlku = "http://www.wisataindonesiaraya.com/getData.php?menu=6&id="+idku;
        JsonArrayRequest req = new JsonArrayRequest(urlku,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject komentar = (JSONObject) response
                                        .get(0);
 
                                nama = komentar.getString("nama_wisata");
                            
                                
                                namawisata.setText(""+nama);
                                deskripsiwisata.setText(Html.fromHtml("<p align=\"justify\">"+deskripsi+"</P>"));
                                loadgambar();
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        showpDialog();
    }
    
    
}
