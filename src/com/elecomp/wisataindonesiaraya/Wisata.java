package com.elecomp.wisataindonesiaraya;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Wisata extends Activity implements OnClickListener{

	Button tombolhome,tombolreload,tomboltentang,tombolkeluar,tombolwisata,tombolartikel,tomboldetail;
	ImageButton menu,cari,wisataalam,wisatabelanja,wisatareligi,wisatasejarah,wisatakeluarga,wisataeco,wisatasemua;
	TextView textku,namawisata;
	int status=1;
	LinearLayout menubar;
	private ProgressDialog pDialog;
	private static String TAG = MainActivity.class.getSimpleName();
	String urlJson="http://wisataindonesiaraya.com/getData.php?menu=3&pilihan=1",nama,deskripsi,urlgbr,id;
	ImageView gambar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_wisata);
		
		tombolhome = (Button) findViewById(R.id.tombolhomewisata);
		tombolhome.setOnClickListener(this);
		tombolreload = (Button) findViewById(R.id.reloadwisata);
		tombolreload.setOnClickListener(this);
		tomboltentang = (Button) findViewById(R.id.tomboltentangwisata);
		tomboltentang.setOnClickListener(this);
		tombolkeluar = (Button) findViewById(R.id.tombolkeluarwisata);
		tombolkeluar.setOnClickListener(this);
		menu = (ImageButton) findViewById(R.id.menuwisata);
		menu.setOnClickListener(this);
		tombolwisata = (Button) findViewById(R.id.tombolwisatawisata);
		tombolwisata.setOnClickListener(this);
		tombolartikel = (Button) findViewById(R.id.tombolartikelwisata);
		tombolartikel.setOnClickListener(this);
		cari = (ImageButton) findViewById(R.id.tombolcariwisata);
		cari.setOnClickListener(this);
		menubar = (LinearLayout) findViewById(R.id.menubarwisata);
		menubar.setVisibility(View.INVISIBLE);
		
		wisataalam=(ImageButton)findViewById(R.id.wisataalam);
		wisataalam.setOnClickListener(this);
		wisatabelanja=(ImageButton)findViewById(R.id.wisatabelanja);
		wisatabelanja.setOnClickListener(this);
		wisatareligi=(ImageButton)findViewById(R.id.wisatareligi);
		wisatareligi.setOnClickListener(this);
		wisatasejarah=(ImageButton)findViewById(R.id.wisatasejarah);
		wisatasejarah.setOnClickListener(this);
		wisataeco=(ImageButton)findViewById(R.id.wisataeco);
		wisataeco.setOnClickListener(this);
		wisatakeluarga=(ImageButton)findViewById(R.id.wisatakeluarga);
		wisatakeluarga.setOnClickListener(this);
		wisatasemua=(ImageButton) findViewById(R.id.wisatasemua);
		wisatasemua.setOnClickListener(this);
		
		/*namawisata=(TextView)findViewById(R.id.namawisatafavorit);
		gambar=(ImageView) findViewById(R.id.gambarwisatafavorit);
		tomboldetail=(Button)findViewById(R.id.detailwisatafavorit);
		tomboldetail.setOnClickListener(this);*/
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);
		
		//ambildata();
	}
	@Override
	public void onClick(View pilihan) {		
		switch (pilihan.getId()) {
		case R.id.menuwisata:
			
			if(status==1)
			{
				tampilmenu();
			}
			if(status==-1)
			{
				hilangmenu();
			}
		status=-status;
		break;
		
		case R.id.tombolwisatawisata:
			Intent wisata = new Intent(Wisata.this, Wisata.class);
			startActivity(wisata);
		break;
		
		case R.id.tombolartikelwisata:
			Intent artikel = new Intent(Wisata.this, Artikel.class);
			startActivity(artikel);
		break;
		
		case R.id.tombolhomewisata:
			Intent baru = new Intent(Wisata.this, Utama.class);
			startActivity(baru);
		break;
		
		case R.id.reloadwisata:
			ambildata();
		break;
		
		case R.id.tomboltentangwisata:
			Intent tentang = new Intent(Wisata.this, Tentang.class);
			startActivity(tentang);
		break;
		
		case R.id.tombolkeluarwisata:
			keluar();
		break;
		
		case R.id.tombolcariwisata:
			Intent cari = new Intent(Wisata.this, Pencarian.class);
			startActivity(cari);
		break;
		
		
		case R.id.wisataalam:
			Intent alam = new Intent(Wisata.this, Hasil.class);
			alam.putExtra("datakirim", "alam");
			startActivity(alam);
		break;
		
		case R.id.wisatabelanja:
			Intent belanja = new Intent(Wisata.this, Hasil.class);
			belanja.putExtra("datakirim", "belanja");
			startActivity(belanja);
		break;
		
		case R.id.wisataeco:
			Intent eco = new Intent(Wisata.this, Hasil.class);
			eco.putExtra("datakirim", "eco");
			startActivity(eco);
		break;
		
		case R.id.wisatakeluarga:
			Intent keluarga = new Intent(Wisata.this, Hasil.class);
			keluarga.putExtra("datakirim", "keluarga");
			startActivity(keluarga);
		break;
		
		case R.id.wisatareligi:
			Intent religi = new Intent(Wisata.this, Hasil.class);
			religi.putExtra("datakirim", "religi");
			startActivity(religi);
		break;
		
		case R.id.wisatasejarah:
			Intent sejarah = new Intent(Wisata.this, Hasil.class);
			sejarah.putExtra("datakirim", "sejarah");
			startActivity(sejarah);
		break;
		
		case R.id.wisatasemua:
			Intent semua = new Intent(Wisata.this, Hasil.class);
			semua.putExtra("datakirim", "semua");
			startActivity(semua);
		break;
		
		/*case R.id.detailwisatafavorit:
			Intent favorit = new Intent(Wisata.this, Halaman.class);
			favorit.putExtra("datakirim", ""+id);
			startActivity(favorit);
		break;*/
		}
	}

	
	public void tampilmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 258, 2);
		animasi.setDuration(300);
		animasi.setFillAfter(true);
		menubar.startAnimation(animasi);
	}
	
	public void hilangmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 2, 858);
		animasi.setDuration(700);
		menubar.startAnimation(animasi);
		//menubar.setVisibility(View.GONE);
		
	}
	
	
	public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(intent);
			    Intent keluar = new Intent(Wisata.this, MainActivity.class);
				startActivity(keluar);
			    android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		dialogkeluar.show();
    }
	
	
	
	private void ambildata() {

        JsonArrayRequest req = new JsonArrayRequest(urlJson,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        
                        try {
                                JSONObject wisata = (JSONObject) response
                                        .get(0);
 
                                nama = wisata.getString("nama_wisata");
                                urlgbr = wisata.getString("foto1_wisata");
                                id=wisata.getString("id_wisata");
                                namawisata.setText(""+nama);
                                loadgambar();
                                
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();
                    }
                });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        showpDialog();  
    }

	private void showpDialog() {
	    if (!pDialog.isShowing())
	        pDialog.show();
	}
	
	private void hidepDialog() {
	    if (pDialog.isShowing())
	        pDialog.dismiss();
	}
	
	public void loadgambar()
    {
    	Picasso.with(this)
        .load("http://wisataindonesiaraya.com/admin/img_foto/"+urlgbr)
        .error(R.drawable.error)
        .resize(110, 100)
        .centerCrop()
        .into(gambar);
    }

}
