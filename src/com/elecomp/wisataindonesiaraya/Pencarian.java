package com.elecomp.wisataindonesiaraya;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Pencarian extends Activity implements OnClickListener{

	Button tombolhome,tombolreload,tomboltentang,tombolkeluar,tombolwisata,tombolartikel,cariwisata,tombolcarilanjut;
	ImageButton menu,cari;
	TextView textku;
	int status=1;
	LinearLayout menubar;
	String nama;
	EditText kotakcari;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_pencarian);
		
		tombolhome = (Button) findViewById(R.id.tombolhomepencarian);
		tombolhome.setOnClickListener(this);
		tombolreload = (Button) findViewById(R.id.reloadpencarian);
		tombolreload.setOnClickListener(this);
		tomboltentang = (Button) findViewById(R.id.tomboltentangpencarian);
		tomboltentang.setOnClickListener(this);
		tombolkeluar = (Button) findViewById(R.id.tombolkeluarpencarian);
		tombolkeluar.setOnClickListener(this);
		menu = (ImageButton) findViewById(R.id.menupencarian);
		menu.setOnClickListener(this);
		tombolwisata = (Button) findViewById(R.id.tombolwisatapencarian);
		tombolwisata.setOnClickListener(this);
		tombolartikel = (Button) findViewById(R.id.tombolartikelpencarian);
		tombolartikel.setOnClickListener(this);
		cari = (ImageButton) findViewById(R.id.tombolcaripencarian);
		cari.setOnClickListener(this);
		menubar = (LinearLayout) findViewById(R.id.menubarpencarian);
		menubar.setVisibility(View.INVISIBLE);
		cariwisata=(Button)findViewById(R.id.tombolcari);
		cariwisata.setOnClickListener(this);
		//tombolcarilanjut=(Button) findViewById(R.id.tombolcarilanjut);
		//tombolcarilanjut.setOnClickListener(this);
		
		kotakcari = (EditText) findViewById(R.id.kotakcari);
		
		textku = (TextView) findViewById(R.id.coba);
			
			
	}
	@Override
	public void onClick(View pilihan) {		
		switch (pilihan.getId()) {
		case R.id.menupencarian:
			
			if(status==1)
			{
				tampilmenu();
			}
			if(status==-1)
			{
				hilangmenu();
			}
		status=-status;
		break;
		
		case R.id.tombolwisatapencarian:
			Intent wisata = new Intent(Pencarian.this, Wisata.class);
			startActivity(wisata);
		break;
		
		case R.id.tombolartikelpencarian:
			Intent artikel = new Intent(Pencarian.this, Artikel.class);
			startActivity(artikel);
		break;
		
		case R.id.tombolhomepencarian:
			Intent home = new Intent(Pencarian.this, Utama.class);
			startActivity(home);
		break;
		
		case R.id.reloadpencarian:
			//ambildata();
		break;
		
		case R.id.tomboltentangpencarian:
			Intent tentang = new Intent(Pencarian.this, Tentang.class);
			startActivity(tentang);
		break;
		
		case R.id.tombolkeluarpencarian:
			keluar();
		break;
		
		case R.id.tombolcaripencarian:
			Intent cari = new Intent(Pencarian.this, Pencarian.class);
			startActivity(cari);
		break;
		
		case R.id.tombolcari:
			nama = kotakcari.getText().toString();
			//textku.setText(""+nama);
			Intent kecari = new Intent(Pencarian.this, Hasil.class);
			kecari.putExtra("pencarian", ""+nama);
			startActivity(kecari);
		break;
		
		//case R.id.tombolcarilanjut:
			//Intent kecarilanjut = new Intent(Pencarian.this, Pencarian2.class);
			//startActivity(kecarilanjut);
		//break;
		}
		
		
	}

	
	public void tampilmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 258, 2);
		animasi.setDuration(300);
		animasi.setFillAfter(true);
		menubar.startAnimation(animasi);
	}
	
	public void hilangmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 2, 858);
		animasi.setDuration(700);
		menubar.startAnimation(animasi);
		//menubar.setVisibility(View.GONE);
		
	}
	
	
	public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(intent);
			    Intent keluar = new Intent(Pencarian.this, MainActivity.class);
				startActivity(keluar);
			    android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		dialogkeluar.show();
    }
	
}




