package com.elecomp.wisataindonesiaraya;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.squareup.picasso.Picasso;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;

public class Artikel extends Activity implements OnClickListener{

	Button tombolhome,tombolreload,tomboltentang,tombolkeluar,tombolwisata,tombolartikel,tomboldetail,tombollainnya;
	ImageButton menu,cari;
	TextView textku;
	int status=1,jumlahdata,dataawal=0,dataakhir=5,i;
	LinearLayout menubar;
	private ProgressDialog pDialog;
	private static String TAG = MainActivity.class.getSimpleName();
	String urlJson="http://wisataindonesiaraya.com/getData.php?menu=4",deskripsi;
	ImageView gambar;
	JSONObject artikel;
	String[] nama;
	String[] urlgbr;
	String[] id;
	LinearLayout layoutku;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_artikel);
		
		tombolhome = (Button) findViewById(R.id.tombolhomeartikel);
		tombolhome.setOnClickListener(this);
		tombolreload = (Button) findViewById(R.id.reloadartikel);
		tombolreload.setOnClickListener(this);
		tomboltentang = (Button) findViewById(R.id.tomboltentangartikel);
		tomboltentang.setOnClickListener(this);
		tombolkeluar = (Button) findViewById(R.id.tombolkeluarartikel);
		tombolkeluar.setOnClickListener(this);
		menu = (ImageButton) findViewById(R.id.menuartikel);
		menu.setOnClickListener(this);
		tombolwisata = (Button) findViewById(R.id.tombolwisataartikel);
		tombolwisata.setOnClickListener(this);
		tombolartikel = (Button) findViewById(R.id.tombolartikelartikel);
		tombolartikel.setOnClickListener(this);
		cari = (ImageButton) findViewById(R.id.tombolcariartikel);
		cari.setOnClickListener(this);
		menubar = (LinearLayout) findViewById(R.id.menubarartikel);
		menubar.setVisibility(View.INVISIBLE);
		tombollainnya = (Button) findViewById(R.id.selanjutnya2);
		tombollainnya.setOnClickListener(this);
		layoutku = (LinearLayout) findViewById(R.id.layoutku);
		
		
		pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading");
        pDialog.setCancelable(false);
		
		ambildata();
	}
	@Override
	public void onClick(View pilihan) {		
		switch (pilihan.getId()) {
		case R.id.menuartikel:
			
			if(status==1)
			{
				tampilmenu();
			}
			if(status==-1)
			{
				hilangmenu();
			}
		status=-status;
		break;
		
		case R.id.tombolwisataartikel:
			Intent wisata = new Intent(Artikel.this, Wisata.class);
			startActivity(wisata);
		break;
		
		case R.id.tombolartikelartikel:
			Intent artikel = new Intent(Artikel.this, Artikel.class);
			startActivity(artikel);
		break;
		
		case R.id.tombolhomeartikel:
			Intent baru = new Intent(Artikel.this, Utama.class);
			startActivity(baru);
		break;
		
		case R.id.reloadartikel:
			ambildata();
		break;
		
		case R.id.tomboltentangartikel:
			Intent tentang = new Intent(Artikel.this, Tentang.class);
			startActivity(tentang);
		break;
		
		case R.id.tombolkeluarartikel:
			keluar();
		break;
		
		case R.id.tombolcariartikel:
			Intent cari = new Intent(Artikel.this, Pencarian.class);
			startActivity(cari);
		break;
		
		case R.id.selanjutnya2:
			if(jumlahdata>dataakhir)
			{
				dataawal=dataawal+5;
				dataakhir=dataakhir+5;
				ambildata();
			}
				

		break;

		}
	}

	
	public void tampilmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 258, 2);
		animasi.setDuration(300);
		animasi.setFillAfter(true);
		menubar.startAnimation(animasi);
	}
	
	public void hilangmenu()
	{
		Animation animasi = new TranslateAnimation(0, 0, 2, 858);
		animasi.setDuration(700);
		menubar.startAnimation(animasi);
		//menubar.setVisibility(View.GONE);
		
	}
	
	
	public void keluar()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KELUAR");
		dialogkeluar.setMessage("Ingin keluar dari aplikasi ?");
		dialogkeluar.setPositiveButton ("Tidak", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.setNegativeButton("Ya", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which )
			{
				Intent intent = new Intent(Intent.ACTION_MAIN);
			    intent.addCategory(Intent.CATEGORY_HOME);
			    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			    startActivity(intent);
			    Intent keluar = new Intent(Artikel.this, MainActivity.class);
				startActivity(keluar);
			    android.os.Process.killProcess(android.os.Process.myPid());
			}
		});
		dialogkeluar.show();
    }
	
	
	
	private void ambildata() {
        JsonArrayRequest req = new JsonArrayRequest(urlJson,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                        	
                        	jumlahdata = response.length();
                            
                        	nama = new String[jumlahdata];
                        	urlgbr = new String[jumlahdata];
                        	id = new String[jumlahdata];
                        	
                        	//for (i = 0; i < response.length(); i++) {
                        	for (i = dataawal; i < dataakhir; i++) {
                                artikel = (JSONObject) response
                                        .get(i);
                                
                                	nama[i] = artikel.getString("judul");
                                    urlgbr[i] = artikel.getString("gambar");
                                    id[i] = artikel.getString("id_artikel");

                                looping2();
                                
                        	}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            /*Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();*/
                        		peringatan();
                        }
 
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        /*VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_SHORT).show();
                        hidepDialog();*/
        
                    		peringatan();

                        
                    }
                });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
        showpDialog();
        
        
    }

	private void showpDialog() {
	    if (!pDialog.isShowing())
	        pDialog.show();
	}
	
	private void hidepDialog() {
	    if (pDialog.isShowing())
	        pDialog.dismiss();
	}
	
	
	public void peringatan()
    {
    	AlertDialog.Builder dialogkeluar=new AlertDialog.Builder(this);
		dialogkeluar.setTitle("KONEKSI");
		dialogkeluar.setMessage("Koneksi Gagal");
		dialogkeluar.setPositiveButton ("OK", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which){
				dialog.dismiss();
			}
		});
		dialogkeluar.show();
		hidepDialog();
    }
	
		
	
	 public void looping2()
	    {
	    	
	    	//LinearLayout Utama
	    	LinearLayout layout1 = new LinearLayout(this);
	    	LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
	    	params.bottomMargin=20;
	    	params.height=0;
	    	params.weight=1.0f;
	    	//params.height=130;
	    	//params.width=300;
	    	params.gravity = Gravity.CENTER;
	    	layout1.setLayoutParams(params);
			layout1.setBackgroundColor(Color.rgb(227, 239, 255));
			layout1.setPadding(10, 10, 10, 10);
			
			//Layout Gambar
			LinearLayout layoutgambar = new LinearLayout(this);
	    	LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
	    	params2.width=0;
	    	params2.weight = 4.0f;
	    	params2.gravity = Gravity.CENTER;
	    	layoutgambar.setLayoutParams(params2);
			
	    	//Gambar
			LinearLayout.LayoutParams GambarParams = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			ImageView gambar = new ImageView(this);
	    	GambarParams.gravity = Gravity.CENTER;
			gambar.setScaleType(ScaleType.CENTER_CROP);
			gambar.setLayoutParams(GambarParams);
			
			
			Picasso.with(this)
	        .load("http://wisataindonesiaraya.com/admin/img_foto/ARTIKEL/"+urlgbr[i])
	        .resize(110, 100)
	        .centerCrop()
	        .into(gambar);
			
			
			//Layout Nama dan Tombol
			LinearLayout layout2 = new LinearLayout(this);
			LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			params3.width=0;
			params3.weight = 6.0f;
			params3.gravity = Gravity.CENTER;
			params3.leftMargin=10;
			layout2.setLayoutParams(params3);
			layout2.setOrientation(LinearLayout.VERTICAL);
			
			//LayoutNama
			LinearLayout layoutnama = new LinearLayout(this);
			LinearLayout.LayoutParams params4 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 0);
			params4.weight = 3.0f;
			params4.gravity = Gravity.CENTER;
			layoutnama.setLayoutParams(params4);
			
			//NamaWisata
			TextView namawisata = new TextView(this);
			namawisata.setText(""+nama[i]);
			namawisata.setTextColor(Color.WHITE);
			namawisata.setTextSize(18);
			namawisata.setTextColor(Color.rgb(64,74,112));
			LinearLayout.LayoutParams params4a = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT );
			params4a.gravity = Gravity.CENTER;
			namawisata.setLayoutParams(params4a);
			
			
			//LayoutTombol
			LinearLayout layouttombol = new LinearLayout(this);
			LinearLayout.LayoutParams params5 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 0);
			params5.weight = 2.0f;
			params5.gravity = Gravity.CENTER;
			layouttombol.setLayoutParams(params5);
			
			//LayoutTombol1a
			LinearLayout layouttombol1a = new LinearLayout(this);
			LinearLayout.LayoutParams params6 = new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT);
			params6.weight = 1.0f;
			params6.gravity = Gravity.CENTER;
			layouttombol1a.setLayoutParams(params6);
			
			//LayoutTombol1b
			LinearLayout layouttombol1b = new LinearLayout(this);
			LinearLayout.LayoutParams params7 = new LinearLayout.LayoutParams(0, LayoutParams.FILL_PARENT);
			params7.weight = 1.0f;
			params7.gravity = Gravity.CENTER;
			layouttombol1b.setLayoutParams(params7);
			
			
			LinearLayout.LayoutParams paramsbtn = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
			paramsbtn.gravity=Gravity.CENTER;
			Button btn = new Button(this);
			//btn.setId(id[i]);
			final String idku = ""+id[i];
			btn.setText("Detail");
			btn.setLayoutParams(paramsbtn);
			btn.setOnClickListener(new OnClickListener() {
		        public void onClick(View v) {
		        	
		        	Intent detail = new Intent(Artikel.this, Halaman2.class);
					detail.putExtra("datakirim", ""+idku);
					startActivity(detail);
		            
		        }
		    });
			
			
			layoutku.addView(layout1);
			layout1.addView(layoutgambar);
			layout1.addView(layout2);
			layout2.addView(layoutnama);
			layout2.addView(layouttombol);
			layoutnama.addView(namawisata);
			layoutgambar.addView(gambar);
			layouttombol.addView(layouttombol1a);
			layouttombol.addView(layouttombol1b);
			layouttombol1b.addView(btn);
	    	
	    	//}
	    }

}


